package com.dalewking.photonexercise.models;

import com.dalewking.photonexercise.data.GridData;

import io.reactivex.Single;

public interface Solver
{
    Single<SolutionResult> solve(GridData<Integer> data);
}
