package com.dalewking.photonexercise.models;

public interface SolutionResult extends Path
{
    boolean isCompletePath();
}
