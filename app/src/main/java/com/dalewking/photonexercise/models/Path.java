package com.dalewking.photonexercise.models;

import java.util.List;

public interface Path
{
    int getCost();

    List<Integer> getRows();
}
