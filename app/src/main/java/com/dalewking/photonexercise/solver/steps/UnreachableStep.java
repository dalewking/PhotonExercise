package com.dalewking.photonexercise.solver.steps;

import com.dalewking.photonexercise.models.Path;
import com.dalewking.photonexercise.solver.SolverStep;

import java.util.List;

class UnreachableStep implements SolverStep
{
    private final Path path;

    UnreachableStep(Path path)
    {
        this.path = path;
    }

    @Override
    public boolean isUnreachable()
    {
        return true;
    }

    @Override
    public SolverStep addColumn(List<Integer> column, int maxCost)
    {
        return this;
    }

    @Override
    public Path getPath()
    {
        return path;
    }
}
