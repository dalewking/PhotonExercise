package com.dalewking.photonexercise.solver.steps;

public class EmptyStep extends BaseStep
{
    @Override
    public PathNode getPath()
    {
        return PathNode.EMPTY;
    }


    PathNode getPredecessorForRow(int row)
    {
        return PathNode.EMPTY;
    }
}
