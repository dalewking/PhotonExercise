package com.dalewking.photonexercise.solver;

import com.dalewking.photonexercise.models.Path;

import java.util.List;

public interface SolverStep
{
    boolean isUnreachable();

    SolverStep addColumn(List<Integer> column, int maxCost);

    Path getPath();
}
