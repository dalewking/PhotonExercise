package com.dalewking.photonexercise.solver.steps;

import android.support.annotation.NonNull;

import com.annimon.stream.Stream;
import com.dalewking.photonexercise.models.Path;
import com.dalewking.photonexercise.utils.Utils;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Nullable;

abstract class PathNode implements Path, Comparable<PathNode>
{
    private final int cost;

    private PathNode(int cost)
    {
        this.cost = cost;
    }

    boolean isReachable()
    {
        return true;
    }

    @Override
    public int getCost()
    {
        return cost;
    }

    abstract PathNode pathTo(int index, int cost, int maxCost);

    @Override
    public int compareTo(@NonNull PathNode o)
    {
        return (cost < o.cost) ? -1 : ((cost == o.cost) ? 0 : 1);
    }

    @NonNull
    private PathNode getPathNode(int index, int cost, int maxCost, ReachablePath predecessor)
    {
        return cost <= maxCost ? new ReachablePath(index, cost, predecessor) : UNREACHABLE;
    }

    private static class ReachablePath extends PathNode
    {
        private final int row;
        private final ReachablePath predecessor;

        ReachablePath(int index, int cost, ReachablePath predecessor)
        {
            super(cost);

            row = index + 1;
            this.predecessor = predecessor;
        }

        @Override
        PathNode pathTo(int index, int cost, int maxCost)
        {
            return super.getPathNode(index, Utils.saturatedAdd(cost, getCost()), maxCost, this);
        }

        @Override
        public List<Integer> getRows()
        {
            List<Integer> rows = Stream.iterate(this, p -> p.predecessor)
                .takeWhile(Utils::nonNull)
                .mapToInt(p -> p.row)
                .collect(LinkedList::new, LinkedList::addFirst);

            return Collections.unmodifiableList(rows);
        }
    }

    static final PathNode EMPTY = new PathNode(0)
    {
        @Override
        PathNode pathTo(int index, int cost, int maxCost)
        {
            return super.getPathNode(index, cost, maxCost, null);
        }

        @Override
        public List<Integer> getRows()
        {
            //noinspection unchecked
            return Collections.EMPTY_LIST;
        }
    };

    static final PathNode UNREACHABLE = new PathNode(Integer.MAX_VALUE)
    {
        @Nullable @Override
        PathNode pathTo(int index, int cost, int maxCost)
        {
            return this;
        }

        @Override
        boolean isReachable()
        {
            return false;
        }

        @Override
        public List<Integer> getRows()
        {
            throw new UnsupportedOperationException();
        }
    };
}
