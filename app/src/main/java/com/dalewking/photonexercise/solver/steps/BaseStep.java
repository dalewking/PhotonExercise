package com.dalewking.photonexercise.solver.steps;

import com.annimon.stream.Stream;
import com.dalewking.photonexercise.solver.SolverStep;

import java.util.List;

abstract class BaseStep implements SolverStep
{
    @Override
    public SolverStep addColumn(List<Integer> column, int maxCost)
    {
        PathNode[] paths = Stream.of(column)
            .map(cost -> cost == null ? Integer.MAX_VALUE : cost)
            .mapIndexed((index, cost) -> getPredecessorForRow(index).pathTo(index, cost, maxCost))
            .toArray(PathNode[]::new);

        return Stream.of(paths).anyMatch(PathNode::isReachable)
            ? new ReachableStep(paths) : new UnreachableStep(getPath());
    }

    abstract PathNode getPredecessorForRow(int row);

    @Override
    public boolean isUnreachable()
    {
        return false;
    }
}
