package com.dalewking.photonexercise.solver.steps;

import com.annimon.stream.ComparatorCompat;
import com.annimon.stream.IntStream;
import com.annimon.stream.Stream;
import com.dalewking.photonexercise.models.Path;
import com.dalewking.photonexercise.solver.SolverStep;
import com.dalewking.photonexercise.utils.Utils;

import java.util.List;

class ReachableStep extends BaseStep
{
    private final PathNode[] paths;

    ReachableStep(PathNode[] paths)
    {
        this.paths = paths;
    }

    @Override
    public SolverStep addColumn(List<Integer> column, int maxCost)
    {
        if(column.size() != paths.length)
        {
            throw new IllegalArgumentException("New column must have same number of rows");
        }

        return super.addColumn(column, maxCost);
    }

    PathNode getPredecessorForRow(int row)
    {
        int n = paths.length;
        return IntStream.of(0, +1, -1)
            // The number of predecessor candidates is limited by number of rows
            // e.g. If there are only 2 rows then +1 and -1 wrap to same index
            .limit(n)
            .map(offset -> row + offset)
            // Handle wrapping
            .map(index -> Utils.mod(index, n))
            .mapToObj(index -> paths[index])
            .min(ComparatorCompat.naturalOrder())
            .orElse(PathNode.UNREACHABLE);
    }

    @Override
    public Path getPath()
    {
        return Stream.of(paths)
            .min(ComparatorCompat.naturalOrder())
            .orElse(PathNode.UNREACHABLE);
    }
}
