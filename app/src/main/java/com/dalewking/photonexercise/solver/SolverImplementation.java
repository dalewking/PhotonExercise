package com.dalewking.photonexercise.solver;

import android.support.annotation.NonNull;

import com.dalewking.photonexercise.data.GridData;
import com.dalewking.photonexercise.models.Path;
import com.dalewking.photonexercise.models.SolutionResult;
import com.dalewking.photonexercise.models.Solver;
import com.google.auto.value.AutoValue;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

public class SolverImplementation implements Solver
{
    private final Callable<SolverStep> startingStep;
    private final int maxCost;

    @Inject
    public SolverImplementation(Callable<SolverStep> startingStep, int maxCost)
    {
        this.startingStep = startingStep;
        this.maxCost = maxCost;
    }

    private SolverStep addColumn(SolverStep step, List<Integer> column)
    {
        return step.addColumn(column, maxCost);
    }

    @Override
    public Single<SolutionResult> solve(GridData<Integer>data)
    {
        return Observable.fromIterable(data.columns())
            .scanWith(startingStep, this::addColumn)
            .takeUntil(SolverStep::isUnreachable)
            .lastOrError()
            .map(Result::create);
    }

    @AutoValue
    abstract static class Result implements SolutionResult
    {
        @NonNull
        static Result create(SolverStep step)
        {
            Path path = step.getPath();
            return new AutoValue_SolverImplementation_Result(
                path.getCost(), path.getRows(), !step.isUnreachable());
        }

    }
}
