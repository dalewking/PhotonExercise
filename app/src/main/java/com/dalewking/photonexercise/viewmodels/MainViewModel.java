package com.dalewking.photonexercise.viewmodels;

import com.dalewking.photonexercise.models.SolutionResult;
import com.dalewking.photonexercise.data.GridData;

import io.reactivex.Observable;

public interface MainViewModel
{
    Observable<GridData<Integer>> getData();

    Observable<SolutionResult> getSolution();

    void changeGrid(GridData<Integer> newGrid);
}
