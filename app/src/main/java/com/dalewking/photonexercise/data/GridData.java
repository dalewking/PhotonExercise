package com.dalewking.photonexercise.data;

import java.io.Serializable;
import java.util.List;

import io.reactivex.Observable;

public interface GridData<T> extends Serializable
{
    int getWidth();
    int getHeight();

    T get(int x, int y);

    void set(int x, int y, T value);

    List<T> column(int column);
    List<T> row(int row);

    Iterable<List<T>> columns();
    Iterable<List<T>> rows();

    Observable<GridData<T>> asObservable();
}
