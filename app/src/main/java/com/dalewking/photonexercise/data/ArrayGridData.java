package com.dalewking.photonexercise.data;

import android.support.annotation.NonNull;

import com.jakewharton.rxrelay2.PublishRelay;

import java.io.IOException;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;

public class ArrayGridData<T> implements GridData<T>
{
    private int width;
    private int height;
    private boolean byRows;
    private T[] data;

    private transient
    PublishRelay<GridData<T>> publisher;

    private ArrayGridData(int width, int height, boolean byRows, T[] data)
    {
        publisher = PublishRelay.create();

        this.width = width;
        this.height = height;
        this.data = data;
        this.byRows = byRows;
    }

    private void readObject(java.io.ObjectInputStream stream)
        throws IOException, ClassNotFoundException
    {
        stream.defaultReadObject();
        publisher = PublishRelay.create();
    }

    @Override
    public int getWidth()
    {
        return width;
    }

    @Override
    public int getHeight()
    {
        return height;
    }

    @Override
    public T get(int x, int y)
    {
        return getUnsafe(checkX(x), checkY(y));
    }

    private T getUnsafe(int x, int y)
    {
        return data[indexOf(x, y)];
    }

    private int checkX(int x)
    {
        if(x < 0 || x >= getWidth())
        {
            throw new IllegalArgumentException("Invalid x " + x);
        }
        else
        {
            return x;
        }
    }

    @Override
    public void set(int x, int y, T value)
    {
        data[indexOf(checkX(x), checkY(y))] = value;

        publisher.accept(this);
    }

    private int checkY(int y)
    {
        if(y < 0 || y >= getHeight())
        {
            throw new IllegalArgumentException("Invalid y " + y);
        }
        else
        {
            return y;
        }
    }

    private int indexOf(int x, int y)
    {
        return byRows ? y * width + x : x * height + y;
    }

    @SuppressWarnings("unused")
    @SafeVarargs
    @NonNull
    public static <T> GridData<T> createByRowsOfWidth(int width, T... data)
    {
        return create(width, data.length / width, true, data);
    }

    @SuppressWarnings("unused")
    @SafeVarargs
    @NonNull
    public static <T> GridData<T> createByColumnsOfHeight(int height, T... data)
    {
        return create(data.length / height, height, false, data);
    }

    @NonNull
    public static <T> GridData<T> create(int width, int height, boolean byRows, T[] data)
    {
        int size = checkSize(width, height);

        if(size != data.length)
        {
            throw new IllegalArgumentException("Incorrect number of data values supplied");
        }

        //noinspection unchecked
        return new ArrayGridData(width, height, byRows, Arrays.copyOf(data, size));
    }

    public static <T> GridData<T> createEmpty(int width, int height)
    {
        int size = checkSize(width, height);

        //noinspection unchecked
        return new ArrayGridData<>(width, height, true, (T[])new Object[size]);
    }

    private static int checkSize(int width, int height)
    {
        if(width <= 0)
        {
            throw new IllegalArgumentException("Width must be greater than zero");
        }

        if(height <= 0)
        {
            throw new IllegalArgumentException("Height must be greater than zero");
        }

        return width * height;
    }

    @Override
    public List<T> column(int column)
    {
        final int x = checkX(column);

        return new AbstractList<T>()
        {
            @Override
            public T get(int y)
            {
                return getUnsafe(x, checkY(y));
            }

            @Override
            public int size()
            {
                return height;
            }
        };
    }

    @Override
    public List<T> row(int row)
    {
        final int y = checkY(row);

        return new AbstractList<T>()
        {
            @Override
            public T get(int x)
            {
                return getUnsafe(checkX(x), y);
            }

            @Override
            public int size()
            {
                return width;
            }
        };
    }

    @Override
    public List<List<T>> columns()
    {
        return new AbstractList<List<T>>()
        {
            @Override
            public List<T> get(int x)
            {
                return column(checkX(x));
            }

            @Override
            public int size()
            {
                return width;
            }
        };
    }

    @Override
    public List<List<T>> rows()
    {
        return new AbstractList<List<T>>()
        {
            @Override
            public List<T> get(int y)
            {
                return row(checkY(y));
            }

            @Override
            public int size()
            {
                return height;
            }
        };
    }

    @Override
    public Observable<GridData<T>> asObservable()
    {
        return publisher;
    }
}
