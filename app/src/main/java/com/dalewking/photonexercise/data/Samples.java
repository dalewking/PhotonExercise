package com.dalewking.photonexercise.data;

import com.annimon.stream.function.Supplier;

public interface Samples
{
    Supplier<GridData<Integer>> SAMPLE_1 = () ->
        ArrayGridData.createByRowsOfWidth(6,
            3, 4, 1, 2, 8, 6,
            6, 1, 8, 2, 7, 4,
            5, 9, 3, 9, 9, 5,
            8, 4, 1, 3, 2, 6,
            3, 7, 2, 8, 6, 4);

    Supplier<GridData<Integer>> SAMPLE_2 = () ->
        ArrayGridData.createByRowsOfWidth(6,
            3, 4, 1, 2, 8, 6,
            6, 1, 8, 2, 7, 4,
            5, 9, 3, 9, 9, 5,
            8, 4, 1, 3, 2, 6,
            3, 7, 2, 1, 2, 3);

    Supplier<GridData<Integer>> SAMPLE_3 = () ->
        ArrayGridData.createByRowsOfWidth(5,
            19, 10, 19, 10, 19,
            21, 23, 20, 19, 12,
            20, 12, 20, 11, 10);

    Supplier<GridData<Integer>> SAMPLE_4 = () ->
        ArrayGridData.createByRowsOfWidth(5,
            5, 8, 5, 3, 5);

    Supplier<GridData<Integer>> SAMPLE_5 = () ->
        ArrayGridData.createByRowsOfWidth(1,
            5,
            8,
            5,
            3,
            5);

    Supplier<GridData<Integer>> SAMPLE_8 = () ->
        ArrayGridData.createByRowsOfWidth(5,
            69, 10, 19, 10, 19,
            51, 23, 20, 19, 12,
            60, 12, 20, 11, 10);

    Supplier<GridData<Integer>> SAMPLE_9 = () ->
        ArrayGridData.createByRowsOfWidth(4,
            60, 3, 3, 6,
             6, 3, 7, 6,
             5, 6, 8, 3);

    Supplier<GridData<Integer>> SAMPLE_10 = () ->
        ArrayGridData.createByRowsOfWidth(4,
            6,  3, -5,  9,
            -5,  2,  4, 10,
             3, -2,  6, 10,
             6, -1, -2, 10);
}
