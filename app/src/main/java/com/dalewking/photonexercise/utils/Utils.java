package com.dalewking.photonexercise.utils;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

public class Utils
{
    /** Computes the true modulus of numerator dividend and denominator divisor where negative
     * values for the dividend wrap around to the end of the range. Note that the sign of the
     * the divisor is ignored.
     *
     * @param dividend
     * @param divisor
     * @return dividend mod divisor where the answer will always be in the range [0 .. |dividend|)
     */
    public static int mod(int dividend, int divisor)
    {
        int remainder = dividend % divisor;

        return remainder < 0 ? remainder + Math.abs(divisor): remainder;
    }

    public static boolean nonNull(Object o)
    {
        return o != null;
    }

    public static String formatPath(Iterable<Integer> iterable)
    {
        return iterable == null ? "[]"
            : Stream.of(iterable)
                .map(String::valueOf)
                .collect(Collectors.joining(" ", "[", "]"));
    }

    public static int saturatedAdd(int a, int b)
    {
        return (int)Math.max(
            Math.min((long)a + b, Integer.MAX_VALUE),
            Integer.MIN_VALUE);
    }

    private Utils() {}
}
