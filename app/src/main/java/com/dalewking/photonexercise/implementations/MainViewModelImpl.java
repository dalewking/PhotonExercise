package com.dalewking.photonexercise.implementations;

import com.dalewking.photonexercise.data.GridData;
import com.dalewking.photonexercise.models.SolutionResult;
import com.dalewking.photonexercise.models.Solver;
import com.dalewking.photonexercise.viewmodels.MainViewModel;
import com.f2prateek.rx.preferences2.Preference;
import com.jakewharton.rxrelay2.BehaviorRelay;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class MainViewModelImpl implements MainViewModel
{
    @Inject
    Solver solver;

    private final BehaviorRelay<GridData<Integer>> currentGrid;

    private final Observable<GridData<Integer>> dataChanges;

    @Inject
    MainViewModelImpl(Preference<GridData<Integer>> dataPreference)
    {
        //noinspection ConstantConditions
        currentGrid = BehaviorRelay.createDefault(dataPreference.get());

        dataChanges = currentGrid
            .switchMap(data -> data.asObservable().startWith(data));

        dataChanges
            .subscribe(dataPreference::set);
    }

    @Override
    public Observable<GridData<Integer>> getData()
    {
        return currentGrid;
    }

    @Override
    public Observable<SolutionResult> getSolution()
    {
        return dataChanges
            .observeOn(Schedulers.computation())
            .flatMapSingle(solver::solve);
    }

    @Override
    public void changeGrid(GridData<Integer> newGrid)
    {
        currentGrid.accept(newGrid);
    }
}
