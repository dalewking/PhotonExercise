package com.dalewking.photonexercise.di;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.nobleworks_software.injection.GenericComponent;
import com.nobleworks_software.injection.android.InjectionService;

import dagger.MembersInjector;

public class ExerciseInjector implements InjectionService.Injector
{
    GenericComponent<AppComponent> appComponent;

    GenericComponent<AppComponent> getAppComponent(Context context)
    {
        if(appComponent == null)
        {
            Application application = (Application) context.getApplicationContext();
            appComponent = new GenericAppComponent(DaggerAppComponent.builder()
                    .appModule(new AppModule(application))
                    .build());
        }

        return appComponent;
    }

    @Override
    public <T> void inject(Context context, T target)
    {
        GenericComponent<?> component = getAppComponent(context);

        MembersInjector<? super T> injector = component.getInjector(target);

        if(injector != null)
        {
            injector.injectMembers(target);
        }
    }
}
