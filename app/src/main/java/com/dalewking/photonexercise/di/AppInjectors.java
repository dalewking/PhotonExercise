package com.dalewking.photonexercise.di;

import com.dalewking.photonexercise.views.MainActivity;

import dagger.MembersInjector;

public interface AppInjectors
{
    MembersInjector<MainActivity> getMainActivityInjector();
}
