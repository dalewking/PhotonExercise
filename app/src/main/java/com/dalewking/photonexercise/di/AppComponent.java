package com.dalewking.photonexercise.di;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ViewModelModule.class} )
interface AppComponent extends AppInjectors
{
}
