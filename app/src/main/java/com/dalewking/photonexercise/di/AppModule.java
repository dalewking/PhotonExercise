package com.dalewking.photonexercise.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Base64;

import com.dalewking.photonexercise.data.ArrayGridData;
import com.dalewking.photonexercise.data.GridData;
import com.dalewking.photonexercise.data.Samples;
import com.dalewking.photonexercise.models.Solver;
import com.dalewking.photonexercise.solver.SolverImplementation;
import com.dalewking.photonexercise.solver.steps.EmptyStep;
import com.f2prateek.rx.preferences2.Preference;
import com.f2prateek.rx.preferences2.RxSharedPreferences;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
class AppModule
{
    private final Application application;

    AppModule(Application application)
    {
        this.application = application;
    }

    @Provides @Singleton
    Application provideApplication()
    {
        return application;
    }

    @Provides @Singleton
    SharedPreferences providePreferences()
    {
        return application.getSharedPreferences(application.getPackageName(), Context.MODE_PRIVATE);
    }

    @Provides @Singleton
    RxSharedPreferences provideRxPreferences(SharedPreferences prefs)
    {
        return RxSharedPreferences.create(prefs);
    }

    @Provides @Singleton
    Solver provideSolver()
    {
        EmptyStep emptyStep = new EmptyStep();
        return new SolverImplementation(() -> emptyStep, 50);
    }

    @Provides @Singleton
    Preference<GridData<Integer>> provideCurrentDataPreference(RxSharedPreferences prefs)
    {
        return prefs.getObject("currentGridData", Samples.SAMPLE_1.get(),
            new Preference.Adapter<GridData<Integer>>()
            {
                @Override
                public GridData<Integer> get(@NonNull String key,
                                             @NonNull SharedPreferences preferences)
                {
                    try
                    {
                        byte[] bytes = Base64.decode(preferences.getString(key, null), Base64.NO_WRAP);
                        ByteArrayInputStream bi = new ByteArrayInputStream(bytes);
                        ObjectInputStream is = new ObjectInputStream(bi);

                        //noinspection unchecked
                        return (GridData<Integer>)is.readObject();
                    }
                    catch (IOException e)
                    {
                        return ArrayGridData.createEmpty(1, 1);
                    }
                    catch (ClassNotFoundException e)
                    {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public void set(@NonNull String key, @NonNull GridData<Integer> value,
                                @NonNull SharedPreferences.Editor editor)
                {
                    try
                    {
                        ByteArrayOutputStream bo = new ByteArrayOutputStream();
                        ObjectOutputStream os = new ObjectOutputStream(bo);

                        os.writeObject(value);
                        os.flush();

                        String s = new String(Base64.encode(bo.toByteArray(), Base64.NO_WRAP));

                        editor.putString(key, s);
                    }
                    catch (IOException e)
                    {
                        throw new RuntimeException(e);
                    }
                }
            });
    }
}
