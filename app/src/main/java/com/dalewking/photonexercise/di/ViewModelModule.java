package com.dalewking.photonexercise.di;

import com.dalewking.photonexercise.implementations.MainViewModelImpl;
import com.dalewking.photonexercise.viewmodels.MainViewModel;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ViewModelModule
{
    @Binds @Singleton
    abstract MainViewModel bindMainViewModel(MainViewModelImpl impl);
}
