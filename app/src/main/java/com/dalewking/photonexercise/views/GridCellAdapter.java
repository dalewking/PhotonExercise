package com.dalewking.photonexercise.views;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dalewking.photonexercise.R;
import com.dalewking.photonexercise.data.GridData;
import com.dalewking.photonexercise.databinding.GridCellBinding;
import com.dalewking.photonexercise.models.SolutionResult;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.trello.rxlifecycle2.android.RxLifecycleAndroid;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

import static android.R.attr.x;

public class GridCellAdapter extends RecyclerView.Adapter<GridCellAdapter.CellViewHolder>
{
    private GridData<Integer> data;

    private Observable<SolutionResult> solution;

    GridCellAdapter(Observable<SolutionResult> solution)
    {
        this.solution = solution;
    }

    void updateData(GridData<Integer> newData)
    {
        data = newData;
        notifyDataSetChanged();
    }

    @Override
    public CellViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        return new CellViewHolder(GridCellBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(CellViewHolder holder, int position)
    {
        int x = position % data.getWidth();
        int y = position / data.getWidth();

        holder.bind(x, y, data.get(x, y));
    }

    @Override
    public int getItemCount()
    {
        return data == null ? 0 : data.getHeight() * data.getWidth();
    }

    public class CellViewHolder extends RecyclerView.ViewHolder
    {
        private final GridCellBinding binding;

        private EditText editor;

        CellViewHolder(GridCellBinding binding)
        {
            super(binding.getRoot());

            editor = (EditText)itemView.findViewById(R.id.cellText);

            this.binding = binding;
        }

        Integer parseText(CharSequence text)
        {
            if(TextUtils.getTrimmedLength(text) == 0)
            {
                return null;
            }
            else
            {
                try
                {
                    return Integer.parseInt(text.toString());
                }
                catch (NumberFormatException e)
                {
                    return null;
                }
            }
        }

        int getColor(int x, int y, SolutionResult result)
        {
            List<Integer> rows = result.getRows();
            if(rows.size() > x && rows.get(x).equals(y + 1))
            {
                return result.isCompletePath() ? R.color.successfulTint : R.color.unsuccessfulTint;
            }
            else
            {
                return android.R.color.transparent;
            }
        }

        void bind(int x, int y, Integer value)
        {
            binding.setValue(value);
            binding.executePendingBindings();

            solution
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxLifecycleAndroid.bindView(itemView))
                .map(solution -> getColor(x, y, solution))
                .subscribe(itemView::setBackgroundResource);

            RxTextView.textChanges(editor)
                .skip(1)
                .compose(RxLifecycleAndroid.bindView(editor))
                .subscribe(newValue -> data.set(x, y, parseText(newValue)));
        }
    }
}
