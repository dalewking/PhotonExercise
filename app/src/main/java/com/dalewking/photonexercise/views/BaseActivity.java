package com.dalewking.photonexercise.views;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ViewStubCompat;

import com.dalewking.photonexercise.BuildConfig;
import com.dalewking.photonexercise.R;
import com.nobleworks_software.injection.android.InjectionService;
import com.trello.navi2.Event;
import com.trello.navi2.component.support.NaviAppCompatActivity;
import com.trello.navi2.rx.RxNavi;
import com.trello.rxlifecycle2.LifecycleProvider;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.trello.rxlifecycle2.RxLifecycle;
import com.trello.rxlifecycle2.android.ActivityEvent;
import com.trello.rxlifecycle2.navi.NaviLifecycle;

import javax.annotation.Nullable;

import butterknife.BindView;

public class BaseActivity extends NaviAppCompatActivity
{
    private final LifecycleProvider<ActivityEvent> provider
            = NaviLifecycle.createActivityLifecycleProvider(this);

    @BindView(R.id.toolbarActionbar)
    Toolbar toolBar;

    @Nullable @BindView(R.id.contentStub)
    ViewStubCompat contentStub;

    protected @LayoutRes int getContentViewWrapper()
    {
        return R.layout.app_bar_layout;
    }

    public <T> LifecycleTransformer<T> bindUntilEvent(ActivityEvent event)
    {
        return provider.bindUntilEvent(event);
    }

    public <T> LifecycleTransformer<T> bindToLifecycle()
    {
        return provider.bindToLifecycle();
    }

    public <T, E> LifecycleTransformer<T> bindUntilEvent(Event<E> event)
    {
        return RxLifecycle.bind(RxNavi.observe(this, event));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG)
        {
            enableStrictMode();
        }

        InjectionService.inject(this);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID)
    {
        super.setContentView(getContentViewWrapper());

        new com.dalewking.photonexercise.views.BaseActivity_ViewBinding(this);

        setSupportActionBar(toolBar);

        contentStub.setLayoutResource(layoutResID);

        contentStub.inflate();
    }

    private void enableStrictMode()
    {
        StrictMode.setThreadPolicy(
                new StrictMode.ThreadPolicy.Builder()
                        .detectAll()
                        .penaltyFlashScreen()
                        .penaltyLog()
                        .build()
        );
        StrictMode.setVmPolicy(
                new StrictMode.VmPolicy.Builder()
                        .detectLeakedSqlLiteObjects()
                        .detectLeakedClosableObjects()
                        .penaltyDropBox()
                        .build()
        );
    }
}
