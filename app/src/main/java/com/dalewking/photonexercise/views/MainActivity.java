package com.dalewking.photonexercise.views;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.dalewking.photonexercise.R;
import com.dalewking.photonexercise.data.ArrayGridData;
import com.dalewking.photonexercise.data.Samples;
import com.dalewking.photonexercise.databinding.ActivityMainBinding;
import com.dalewking.photonexercise.databinding.GridSizeLayoutBinding;
import com.dalewking.photonexercise.utils.FixedGridLayoutManager;
import com.dalewking.photonexercise.viewmodels.MainViewModel;
import com.example.android.recyclerplayground.GridDividerDecoration;

import javax.inject.Inject;

import butterknife.BindView;

public class MainActivity extends BaseActivity
{
    @BindView(R.id.grid)
    RecyclerView grid;

    @Inject
    MainViewModel viewModel;
    private ActivityMainBinding binding;
    private FixedGridLayoutManager layout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new MainActivity_ViewBinding(this);

        binding = ActivityMainBinding.bind(findViewById(R.id.activity_main));
        layout = new FixedGridLayoutManager();

        GridCellAdapter gridCellAdapter = new GridCellAdapter(viewModel.getSolution());

        grid.setLayoutManager(layout);
        grid.setAdapter(gridCellAdapter);
        grid.setHasFixedSize(true);
        grid.setVerticalScrollBarEnabled(true);
        grid.addItemDecoration(new GridDividerDecoration(this));

        viewModel.getData()
            .doOnNext(data -> layout.setTotalColumnCount(data.getWidth()))
            .compose(bindToLifecycle())
            .doOnNext(gridCellAdapter::updateData)
            .subscribe(data -> grid.requestLayout());

        viewModel.getSolution()
            .compose(bindToLifecycle())
            .subscribe(binding::setSolution);
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.newGrid: getNewSize();
                return true;
            case R.id.sample1: viewModel.changeGrid(Samples.SAMPLE_1.get());
                return true;
            case R.id.sample2: viewModel.changeGrid(Samples.SAMPLE_2.get());
                return true;
            case R.id.sample3: viewModel.changeGrid(Samples.SAMPLE_3.get());
                return true;
            case R.id.sample4: viewModel.changeGrid(Samples.SAMPLE_4.get());
                return true;
            case R.id.sample5: viewModel.changeGrid(Samples.SAMPLE_5.get());
                return true;
            case R.id.sample8: viewModel.changeGrid(Samples.SAMPLE_8.get());
                return true;
            case R.id.sample9: viewModel.changeGrid(Samples.SAMPLE_9.get());
                return true;
            case R.id.sample10: viewModel.changeGrid(Samples.SAMPLE_10.get());
                return true;
        }

        return false;
    }

    void getNewSize()
    {
        GridSizeLayoutBinding binding = GridSizeLayoutBinding.inflate(getLayoutInflater());

        binding.setColumns(4);
        binding.setRows(4);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(binding.getRoot())
            .setTitle(R.string.setSizeText)
            .setPositiveButton(android.R.string.ok,
                (dialog, id) -> viewModel.changeGrid(
                    ArrayGridData.createEmpty(binding.getRows() + 1, binding.getColumns() + 1)))
            .setNegativeButton(android.R.string.cancel, null)
            .show();
    }
}
