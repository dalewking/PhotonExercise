package com.dalewking.photonexercise.core;

import android.app.Application;

import com.dalewking.photonexercise.di.ExerciseInjector;
import com.nobleworks_software.injection.android.InjectionService;

public class ExerciseApplication extends Application
{
    @Override
    public void onCreate()
    {
        super.onCreate();

        InjectionService.setInjector(new ExerciseInjector());
    }
}
