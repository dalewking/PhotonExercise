package com.dalewking.photonexercise.testing;

import org.hamcrest.CustomTypeSafeMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class TestUtil
{
    public static <T> Matcher<Class<T>> isAnInterface()
    {
        return new CustomTypeSafeMatcher<Class<T>>("an interface")
        {
            @Override
            protected boolean matchesSafely(Class<T> clazz)
            {
                return clazz.isInterface();
            }

            @Override
            protected void describeMismatchSafely(Class<T> item, Description mismatchDescription)
            {
                mismatchDescription.appendValue(item).appendText(" was a class");;
            }
        };
    }

    public static <T> Matcher<Class<T>> isAPublicClass()
    {
        return new CustomTypeSafeMatcher<Class<T>>("class to be public")
        {
            @Override
            protected boolean matchesSafely(Class<T> clazz)
            {
                return Modifier.isPublic(clazz.getModifiers());
            }

            @Override
            protected void describeMismatchSafely(Class<T> item, Description mismatchDescription)
            {
                mismatchDescription.appendValue(item).appendText(" is not public");
            }
        };
    }

    public static <T extends Member> Matcher<T> isPublicMember()
    {
        return new CustomTypeSafeMatcher<T>("member to be public")
        {
            @Override
            protected boolean matchesSafely(T m)
            {
                return Modifier.isPublic(m.getModifiers());
            }

            @Override
            protected void describeMismatchSafely(T item, Description mismatchDescription)
            {
                mismatchDescription.appendValue(item).appendText(" is not public");
            }
        };
    }

    public static Matcher<Method> hasReturnType(Class<?> returnType)
    {
        return new CustomTypeSafeMatcher<Method>("method to return " + returnType)
        {
            @Override
            protected boolean matchesSafely(Method m)
            {
                return returnType.isAssignableFrom(m.getReturnType());
            }

            @Override
            protected void describeMismatchSafely(Method item, Description mismatchDescription)
            {
                mismatchDescription.appendValue(item)
                        .appendText(" returns ")
                        .appendValue(item.getReturnType());
            }
        };
    }

    private TestUtil() {}
}
