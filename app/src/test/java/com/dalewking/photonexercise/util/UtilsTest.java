package com.dalewking.photonexercise.util;

import android.annotation.SuppressLint;

import com.dalewking.photonexercise.utils.Utils;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(DataProviderRunner.class)
public class UtilsTest
{
    @DataProvider
    public static Object[][] modTestCases()
    {
        return new Object[][]
        {
            {0, 4, 0},
            {1, 4, 1}, {-1, 4, 3},
            {2, 4, 2}, {-2, 4, 2},
            {3, 4, 3}, {-3, 4, 1},
            {4, 4, 0}, {-4, 4, 0},
            {5, 4, 1}, {-5, 4, 3},
            {6, 4, 2}, {-6, 4, 2},
            {7, 4, 3}, {-7, 4, 1},
            {8, 4, 0}, {-8, 4, 0},
            {9, 4, 1}, {-9, 4, 3},
            {0, 3, 0},
            {1, 3, 1}, {-1, 3, 2},
            {2, 3, 2}, {-2, 3, 1},
            {3, 3, 0}, {-3, 3, 0},
            {4, 3, 1}, {-4, 3, 2},
            {0, -4, 0},
            {1, -4, 1}, {-1, -4, 3},
            {2, -4, 2}, {-2, -4, 2},
            {3, -4, 3}, {-3, -4, 1},
            {4, -4, 0}, {-4, -4, 0},
            {5, -4, 1}, {-5, -4, 3},
            {6, -4, 2}, {-6, -4, 2},
            {7, -4, 3}, {-7, -4, 1},
            {8, -4, 0}, {-8, -4, 0},
            {9, -4, 1}, {-9, -4, 3},
            {0, -3, 0},
            {1, -3, 1}, {-1, -3, 2},
            {2, -3, 2}, {-2, -3, 1},
            {3, -3, 0}, {-3, -3, 0},
            {4, -3, 1}, {-4, -3, 2},
        };
    }

    @Test
    @UseDataProvider("modTestCases")
    public void testModFunction(int numerator, int denominator, int expectedResult)
    {
        assertThat(String.format("Utils.mod(%d, %d)", numerator, denominator),
                Utils.mod(numerator, denominator), is(expectedResult));
    }

    @DataProvider(format = "%m%p[1..-1]")
    public static Object[][] formatPathTestCases()
    {
        return new Object[][]
            {
                {"[]", },
                {"[5]", 5},
                {"[3 7 8]", 3, 7, 8},
            };
    }

    @Test
    @UseDataProvider("formatPathTestCases")
    public void formatPathShouldFormatCorrectly(String output, Integer... input)
    {
        Iterable<Integer> iterable = Arrays.asList(input);
        assertThat(Utils.formatPath(iterable), equalTo(output));
    }

    @Test
    public void formatPathShouldHandleNull()
    {
        assertThat(Utils.formatPath(null), equalTo("[]"));
    }

    @DataProvider(format = "%m[%p[0] + %p[1] equals %p[2]]")
    public static Object[][] saturatedAddTestCases()
    {
        return new Object[][]
            {
                {0, 0, 0}, {1, 0, 1}, {2, 3, 5}, {-1, 0, -1},
                {Integer.MAX_VALUE, 0, Integer.MAX_VALUE},
                {Integer.MAX_VALUE - 1, 1, Integer.MAX_VALUE},
                {Integer.MIN_VALUE, 0, Integer.MIN_VALUE},
                {Integer.MIN_VALUE + 1, -1, Integer.MIN_VALUE},
                {Integer.MAX_VALUE, Integer.MIN_VALUE, -1},
                {Integer.MAX_VALUE, 1, Integer.MAX_VALUE},
                {Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE},
                {Integer.MAX_VALUE / 2, Integer.MAX_VALUE, Integer.MAX_VALUE},
                {Integer.MIN_VALUE, -1, Integer.MIN_VALUE},
                {Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE},
                {Integer.MIN_VALUE / 2, Integer.MIN_VALUE, Integer.MIN_VALUE},
            };
    }

    @SuppressLint("DefaultLocale")
    @Test
    @UseDataProvider("saturatedAddTestCases")
    public void testSaturatedAdd(int a, int b, int expectedResult)
    {
        assertThat(String.format("Utils.saturatedAdd(%d, %d)", a, b),
            Utils.saturatedAdd(a, b), equalTo(expectedResult));
    }
}
