package com.dalewking.photonexercise.models;

import org.junit.Test;

import static com.dalewking.photonexercise.testing.TestUtil.isAPublicClass;
import static com.dalewking.photonexercise.testing.TestUtil.isAnInterface;
import static org.hamcrest.MatcherAssert.assertThat;

public abstract class BaseModelTest<T>
{
    protected final Class<T> classUnderTest;

    public BaseModelTest(Class<T> classUnderTest)
    {
        this.classUnderTest = classUnderTest;
    }

    @Test
    public void shouldBeAPublicInterface()
    {
        assertThat(classUnderTest, isAnInterface());
        assertThat(classUnderTest, isAPublicClass());
    }
}
