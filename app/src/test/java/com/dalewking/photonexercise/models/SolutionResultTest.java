package com.dalewking.photonexercise.models;

import org.junit.Test;

import java.lang.reflect.Method;

import static com.dalewking.photonexercise.testing.TestUtil.hasReturnType;
import static com.dalewking.photonexercise.testing.TestUtil.isPublicMember;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class SolutionResultTest extends BaseModelTest<SolutionResult>
{
    public SolutionResultTest()
    {
        super(SolutionResult.class);
    }

    @Test
    public void shouldHaveIsCompletePathMethod() throws Exception
    {
        Method method = classUnderTest.getMethod("isCompletePath");

        assertThat(method, isPublicMember());
        assertThat(method, hasReturnType(Boolean.TYPE));
    }

    @Test
    public void shouldHaveGetCostMethod() throws Exception
    {
        Method method = classUnderTest.getMethod("getCost");

        assertThat(method, isPublicMember());
        assertThat(method, hasReturnType(Integer.TYPE));
    }

    @Test
    public void shouldHaveGetRowsMethod() throws Exception
    {
        Method method = classUnderTest.getMethod("getRows");

        assertThat(method, isPublicMember());
        assertThat(method.getGenericReturnType().toString(),
            equalTo("java.util.List<java.lang.Integer>"));
    }
}
