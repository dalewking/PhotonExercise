package com.dalewking.photonexercise.solver;

import com.dalewking.photonexercise.data.ArrayGridData;
import com.dalewking.photonexercise.data.GridData;
import com.dalewking.photonexercise.data.Samples;
import com.dalewking.photonexercise.models.SolutionResult;
import com.dalewking.photonexercise.models.Solver;
import com.dalewking.photonexercise.solver.steps.EmptyStep;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.hasProperty;

@RunWith(DataProviderRunner.class)
public class SolverImplementationTest
{
    @DataProvider(format = "%m[%p[0]]")
    public static Object[][] sampleProblems()
    {
        return new Object[][]
            {
                {
                    "Sample 1: (6X5 matrix normal flow)", Samples.SAMPLE_1.get(), 50,
                    true, 16, contains(1, 2, 3, 4, 4, 5)
                },
                {
                    "Sample 2: (6X5 matrix normal flow)", Samples.SAMPLE_2.get(), 50,
                    true, 11, either(contains(1, 2, 1, 5, 4, 5)).or(contains(1, 2, 1, 5, 5, 5))
                },
                {
                    "Sample 3: (5X3 matrix with no path <50)", Samples.SAMPLE_3.get(), 50,
                    false, 48, contains(1, 1, 1)
                },
                {
                    "Sample 4: (1X5 matrix)", Samples.SAMPLE_4.get(), 50,
                    true, 26, contains(1, 1, 1, 1, 1)
                },
                {
                    "Sample 5: (5X1 matrix)", Samples.SAMPLE_5.get(), 50,
                    true, 3, contains(4)

                },
                {
                    "Sample 8: (Starting with >50)", Samples.SAMPLE_8.get(), 50,
                    false, 0, emptyIterable()
                },
                {
                    "Sample 9: (One value >50)", Samples.SAMPLE_9.get(), 50,
                    true, 14, either(contains(3, 2, 1, 3)).or(contains(3, 1, 1, 3))
                },
                {
                    "Sample 10: (Negative values)", Samples.SAMPLE_10.get(), 50,
                    true, 0, contains(2, 3, 4, 1)
                },
                {
                    "Boundary Check: (starting with 50, limit 50)",
                    ArrayGridData.createByRowsOfWidth(2,
                        50, -3,
                        50, -2),
                    50, true, 47, either(contains(1, 1)).or(contains(2, 1))
                },
                {
                    "Boundary Check: (starting with 50, limit 49)",
                    ArrayGridData.createByRowsOfWidth(2,
                        50, -3,
                        50, -2),
                    49, false, 0, emptyIterable()
                },
                {
                    "Boundary Check: (starting with 51, limit 50)",
                    ArrayGridData.createByRowsOfWidth(2,
                        51, -3,
                        51, -2),
                    50, false, 0, emptyIterable()
                },
                {
                    "Boundary Check: (starting with 51, limit 51)",
                    ArrayGridData.createByRowsOfWidth(2,
                        51, -3,
                        51, -2),
                    51, true, 48, either(contains(1, 1)).or(contains(2, 1))
                },
                {
                    "Limit Check 1",
                    ArrayGridData.createByRowsOfWidth(2,
                        -40, 55),
                    50, true, 15, contains(1, 1)
                },
                {
                    "Overflow Check 1",
                    ArrayGridData.createByRowsOfWidth(2,
                        3, Integer.MAX_VALUE),
                    50, false, 3, contains(1)
                },
                {
                    "Overflow Check 2",
                    ArrayGridData.createByRowsOfWidth(2,
                        -30, Integer.MIN_VALUE),
                    50, true, Integer.MIN_VALUE, contains(1, 1)
                },
            };
    }

    @Test
    @UseDataProvider("sampleProblems")
    public void shouldProduceCorrectResult(@SuppressWarnings("UnusedParameters") String name,
                                           GridData<Integer> matrix,
                                           int maxCost,
                                           boolean complete, int cost,
                                           Matcher<Iterable<Integer>> expectedPath)
    {
        SolverStep empty = new EmptyStep();
        Solver solver = new SolverImplementation(() -> empty, maxCost);

        SolutionResult result = solver.solve(matrix)
                .blockingGet();

        MatcherAssert.assertThat(result, hasProperty("completePath", is(complete)));
        MatcherAssert.assertThat(result, hasProperty("cost", is(cost)));
        MatcherAssert.assertThat(result, hasProperty("rows", expectedPath));
    }
}
