The objective is to find the path of lowest cost when moving across a grid. For this challenge,
you are provided a grid of integers where each integer represents the amount of cost encountered
at a given point on the grid. A path enters the grid from the left (at any point) and passes through
the grid to the right, moving only one column per round. Movement is always to the same row or
an adjacent row, meaning the path can proceed horizontally or diagonally. For the sake of this
challenge, we assume the first and last row are also adjacent. Effectively, the grid “wraps”.

The total cost of a path is the sum of the integers in each of the visited cells. The solution needs
to handle grids of various sizes with a minimum of 1 row and 5 columns up to 10 rows and 100
columns. If in the next move, the total cost will exceed 50, that path is abandoned.
The purpose of this challenge is to find the path of least cost (that is, the path with the lowest
total cost of any possible path).

Three lines should be output for each matrix specification. The first line is either “Yes” or “No”
to indicate the path made it all the way through the grid. The second line is the total cost. The
third line shows the path taken as a sequence of n delimited integers, each representing the rows
traversed in turn. If there is more than one path of least cost, only one path need be shown in the 
solution.
